#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 14 17:37:46 2018

@author: yoobin
"""

#%matplotlib inline
import time
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

# (1) c.jpg
a = mpimg.imread('a.jpg')
b = mpimg.imread('b.jpg')
a_copy = a.copy()             # a is read-only; create a copy
a_copy[250:650, 100:500] = b
plt.imshow(a_copy)
plt.imsave('c.jpg',a_copy)


# (2) i.jpg
# (430, 400, 3)
g = mpimg.imread('g.jpg').copy()
h = mpimg.imread('h.jpg').copy()

sig_g = g.astype(np.int16)
sig_h = h.astype(np.int16)
diff = sig_g - sig_h
diff_row = [r for r in diff]

for i in range(430):
    for j in range(400):
        if (np.absolute(diff_row[i][j]) <= [30,30,30]).all():
            diff_row[i][j] = [0,0,0]
        else:
            diff_row[i][j] = h[i][j]

diff = diff.astype(np.uint8)
plt.imshow(diff)
plt.imsave('i.jpg',diff)


## (3-1) f.jpg : Black background
#e = mpimg.imread('e.jpg').copy()
#e_row = [r for r in e]
#for i in range(161):
#    for j in range(165):
#        if e_row[i][j][0] < 200 and e_row[i][j][1] > 200 :
#            e_row[i][j] = np.array([0,0,0])
#
## (3-2) f.jpg : Times Square
## d.shape : (750, 1280, 3)
## e.shape : (161, 165, 3)
#d = mpimg.imread('d.jpg').copy()
#for i in range(570, 731):
#    for j in range(245, 406):
#        if (e_row[i-570][j-245] != [0,0,0]).any():
#            d[i][j] = e_row[i-570][j-245]
#plt.imshow(d)
#plt.imsave('f.jpg',d)



        

